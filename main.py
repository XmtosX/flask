from flask import  Flask

app = Flask(__name__)

@app.route('/')
def home_page():
    return "<h1>Hello SNA Team</h1> <p>This is my home page</p>"

if __name__ == "__main__":
    app.run(port=5000,host="0.0.0.0")