FROM python:3.8
WORKDIR /flask
COPY . /flask/
RUN pip install -r req.txt
ENTRYPOINT [ "python", "main.py" ]